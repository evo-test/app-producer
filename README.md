### App Producer
[![pipeline status](https://gitlab.com/evo-test/app-producer/badges/master/pipeline.svg)](https://gitlab.com/evo-test/app-producer/-/commits/master)

Simple microservice based on Moleculer, that produces msg containing unix timestamp to Kafka topic 'input', once per second

<a href="https://gitlab.com/evo-test/deployments/-/blob/master/README.md">Setup instructions</a>