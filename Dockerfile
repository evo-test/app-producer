FROM node:12-alpine

RUN apk update && apk add python make g++ && rm -rf /var/cache/apk/*

ENV NODE_ENV=production

RUN mkdir /app
WORKDIR /app

COPY index.js .
COPY package.json .
COPY package-lock.json .

RUN npm install --production

CMD ["node", "index.js"]
