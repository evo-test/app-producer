"use strict";

const { ServiceBroker } = require('moleculer');
const {KafkaClient, HighLevelProducer} = require('kafka-node');
const PrometheusService = require('moleculer-prometheus');

const KAFKA_HOST = process.env.KAFKA_HOST || 'kafka.vagrant:9092';
const KAFKA_TOPIC = process.env.KAFKA_TOPIC || 'input';
const PROM_PORT = process.env.PROM_PORT || 3030;

const broker = new ServiceBroker({
    logger: {
        type: 'Console',
        options: {
            level: 'info',
            colors: false,
            moduleColors: false,
            formatter: 'json',
            objectPrinter: null,
            autoPadding: false
        }
    }
});

broker.createService({
    mixins: [PrometheusService],
    settings: {
        port: PROM_PORT,
        collectDefaultMetrics: true,
        metrics: {
            'producer_input': { type: 'Counter', help: 'Producer input topic, requests rate' }
        }
    }
});

broker.createService({
    name: 'producer',
    settings: {
        sendInterval: 1000,
        kafka: {
            host: KAFKA_HOST,
            topic: KAFKA_TOPIC
        }
    },
    actions: {},
    methods: {
        async init() {
            const kafkaClient = await this.createClient();
            this.logger.info('Connected to kafka broker');

            const kafkaProducer = new HighLevelProducer(kafkaClient, {
                requireAcks: 0
            });
            await this.createTopics(kafkaProducer, [this.settings.kafka.topic]);
            this.logger.info('Topics created');

            this.sendIntervalId = setInterval(() => {
                const payload = Date.now();
                this.sendMessage(kafkaProducer, [{
                    topic: this.settings.kafka.topic,
                    messages: payload
                }]).then((res) => {
                    this.logger.info('Message successfully sent', payload);
                }).catch((e) => {
                    this.logger.error('Send message error', e);
                });
            }, this.settings.sendInterval);

            return new Promise(() => {});
        },

        createClient() {
            this.logger.info('Connecting...');
            return new Promise((resolve, reject) => {
                const kafkaClient = new KafkaClient({
                    'kafkaHost': this.settings.kafka.host
                });

                kafkaClient.on('ready', () => resolve(kafkaClient));
                kafkaClient.on('error', (e) => reject(e));
            });
        },
        createTopics(kafkaProducer, topic) {
            return new Promise((resolve, reject) => {
                kafkaProducer.createTopics(topic, true, (err, success) => {
                    if (err) return reject(err);
                    resolve(success);
                });
            });
        },
        sendMessage(kafkaProducer, payload) {
            broker.broadcast('metrics.update', {
                name: 'producer_input',
                method: 'inc',
                value: 1
            });

            return new Promise((resolve, reject) => {
                kafkaProducer.send(payload, (err, success) => {
                    if (err) return reject(err);
                    resolve(success);
                });
            });
        }
    },

    created() {
        this.sendIntervalId = null;
    },
    started() {
        this.init();
    },
    stopped() {
        if (this.sendIntervalId) {
            clearInterval(this.sendIntervalId);
            this.sendIntervalId = null;
        }
    }
});

broker.start().then(() => {
    broker.repl();
});